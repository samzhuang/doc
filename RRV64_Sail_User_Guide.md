# Sail User's Guide

## 1. How to Install Sail on CentOS 

### 1.1 Install Sail Online

#### Clone from remote

Download the following sail repositories:

```
$ git clone https://github.com/rems-project/sail-riscv.git
```

#### Install required libraries

Install bubblewarp

```
$ sudo yum install bubblewrap
```

#### Install opam 

on .5 server with CentOS version : 

```
$ cat /etc/centos-release
CentOS Linux release 7.9.2009 (Core) 
```

Install opam:

The quickest way to get the latest opam up and working is to run this script:

```
sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)
```

This script will guide you installing the latest version opam, and you could use the following command to check opam version after installation:

First thing to et the enviromental variable:

```
eval $(opam env)
```

```
$ which opam 
/usr/local/bin/opam

$ opam --version 
2.0.8 5 
```

#### Install ocaml

```
$opam init
```

Use `ocaml -version` to check your OCaml version. 

```
$ ocaml -version 
The OCaml toplevel, version 4.12.0 

$ eval $(opam config env)
```

#### Install z3

Download the Z3 repository and install Z3:

```
$ git clone https://github.com/angr/z3.git 
$ cd z3 
$ python scripts/mk_make.py 
$ cd build/ 
$ make 
$ sudo -s 
$ make install 
```

#### Install sail

```
opam install sail
```

Follow the sail install guide and once the installation has been done, check sail version:

```
$ eval $(opam env) 

$ which sail 
~/.opam/default/bin/sail 

$ sail -v 
Sail 0.14 (sail2 @ opam) 
```

### 1.2 Install Sail Offline

The offline packages are pre-downloaded in the sail_offlin.tar. 

*Note: the sail must be installed in the home directory.*

```
$ mkdir /home/sail/
$ tar -xf sail_offline.tar
```

#### Install bubblewrap

```
$ cd /home/sail

$ xz -d bubblewrap-0.5.0.tar.xz
$ tar -xf bubblewrap-0.5.0.tar
$ chmod a+x bwrap

$ export PATH=$PATH:/home/sail/bubblewrap-0.5.0/completions/bash/
```

#### Install opam

Firstly put the opam under sail PATH. 

```
$ cd /home/sail
$ tar -xf opam.tar 
$ opam --version
$ opam init
```

#### Install ocaml

Opam switch create ocaml.4.12.0

```
$ eval $(opam env --switch=ocaml.4.12.0)
```

#### Install z3

```
$ tar -xf z3.tar
$ cd z3
$ python3 scripts/mk_make.py
$ cd build/
$ make
$ sudo make install
```

## 2. Build Simulator

On sail-riscv folder:

```
$ ./build_simulators.sh
```

If you encountered the following problem:

```
Error: 
SMT solver returned unexpected status 126 
/bin/sh: /usr/bin/z3: Permission denied 
make: *** [Makefile:189: generated_definitions/ocaml/RV32/riscv.ml] Error 1 
Failure to execute: make ARCH=RV32 ocaml_emulator/riscv_ocaml_sim_RV32 
```

Try:

```
$ sudo chmod -R 777 /usr/bin/z3
```

Then try to build simulator again.

## 3. Run Test

First make sure the c emulator has been generated:

```
$ ll sail-riscv/c_emulator/riscv_sim 
riscv_sim.c     riscv_sim_RV32  riscv_sim_RV64 
```

Then run a test case using the generated emulator:

```
$ ./c_emulator/riscv_sim_RV64 test/riscv-tests/rv64ui-p-srl.elf
```

SUCCESS shown at the end indicate a successful run for the elf.