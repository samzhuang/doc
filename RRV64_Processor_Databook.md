#  RiVAI RRV64 Processor Databook

## 1    Overview

RRV64 is a 64-bit RISC-V Core designed for embedded applications. It has a 5 stage in-order pipeline and 1 level instruction cache and shareable L2 data cache. RRV64 supports RV64IMAFDC(RV64GC) instruction sets, Sv39 Virtual Address format, legal combinations of privilege modes in conjunction with Physical Memory Protection (PMP). It is capable of running a full-featured operating system like Linux. The core is compatible with all applicable RISC‑V standards.

RRV64 is designed to be feature a very flexible memory system that includes L1 caches, L2 caches(currently only has L1 instruction cache), bus interfaces, and memory maps that provide a lot of flexibility for SoC integration.

### 1.1   Feature

- RV64IMAFDC instruction sets supporting
- Up to 4 cores supporting
- Multi-level cache system including  private L1  I caches and shareable L2 data cache.
- MMU and SV39 Virtual Address format supporting
- Linux Supporting

### 1.2   Block diagram

The RRV64 SoC contains up to 4 64-bit RISCV cores, a on-chip L2 SRAM, and a debug module. [[郑1\]](#_msocom_1) The cores and L2 sram are connected by CPU_NOC.

The block diagram of the whole system is shown below.

![](assets/RRV64_Processor_Databook/block-diagram.png)

### 1.3   Core configurations

| Design Parameter        | Default Value | Description                                                  |
| ----------------------- | ------------- | ------------------------------------------------------------ |
| CORE_NUM                | 1             | RRV64 core number in the RRV64  subsystem, default number is 1 |
| RRV64_PHY_ADDR_WIDTH    | 56            | physical address width                                       |
| RRV64_VIR_ADDR_WIDTH    | 39            | virtual address width                                        |
| RRV64_ICACHE_SIZE_KBYTE | 8             | L1 icache size in KByte                                      |
| RRV64_N_ICACHE_WAY      | 2             | L1 icache way numbers                                        |
| RRV64_N_ICACHE_PREFETCH | 4             | L1 icache cacheline prefetch request numbers                 |
| RRV64_NUM_PAGE_LEVELS   | 3             | levels of of page                                            |
| RRV64_PAGE_SIZE_BYTE    | 4096          | page size in bytes                                           |
| RRV64_ASID_WIDTH        | 16            | constants for MMU                                            |
| RRV64_PPN_WIDTH         | 44            | constants for MMU                                            |
| RRV64_VPN_WIDTH         | 27            | constants for MMU                                            |
| RRV64_PPN_PART_WIDTH    | 9             | constants for MMU                                            |
| RRV64_VPN_PART_WIDTH    | 9             | constants for MMU                                            |
| RRV64_NUM_ITLB_ENTRIES  | 8             | constants for MMU                                            |
| RRV64_NUM_DTLB_ENTRIES  | 8             | constants for MMU                                            |
| RRV64_ITB_ADDR_WIDTH    | 5             | instruction trace buffer memory address width                |
| RRV64_CNTR_WIDTH        | 48            | csr counter width                                            |

 

# 2    Pipeline Stages

The RRV64 has a 5-stage in-order pipeline: fetch, decode, execute, MEM access, write back. The block diagram of the whole system is shown below.

![](assets/RRV64_Processor_Databook/core-diagram.png)



## 2.1 IF (Fetch)

### 2.1.1 Introduce

Instruction Fetch (RRV64_fetch) is the first pipeline stage in RRV64. This stage is responsible for initiating requests for instruction data by sending requests to the instruction buffer. If it hits, the instruction data will be available in the next cycle. Otherwise, the instruction buffer will send a request to I-Cache to obtain the instruction data. Such process will take several cycles of delay. The IF module is also responsible for generating the address of the next instruction. It receives PC requests from other pipeline stages and arbitrates using a fixed priority scheme. The modules that act as PC sources are listed below, from the highest priority to the lowest..

l RRV64_csr: Sends PC on exceptions, interrupts and trap return instructions.

l rrv_mem_access: Sends PC when completing a fence.i instruction and when some of CSR registers have been modified. For fences, the PC request is delayed until all fetches before the fence instruction are completed and I-Cache is flushed. This is in case of any self-modifying code. For CSR modifications, delaying the PC request ensures that the CSR operation will use the correct values.

l RRV64_execute: Sends PC when a branch instruction taken.

l rrv_fetch: Sends PC for the normal case (next PC=PC+4 or PC+2 for compressed instructions), immediate jumps and register jumps.

IF stage also has a predecoder that recognizes JAL instructions, fence_i, mret, or a csr operation on the PMP related registers and the instructions that need to read registers. For JAL, it generates corresponding redirect request to pc mux. For fence_i, mret, etc. it generates stall signal to stall IF stage until the instruction is retired. For instruction which need to read registers, it generates register read requests to regfile.

### 2.1.2 Instruction Buffer

The instruction buffer is mainly used to prefetch instructions from L1 Cache. In addition to the instruction requested by the IF, the instruction buffer also fetches the instructions of the next two cache lines. If the execution flow is sequential, or there is a forward jump whose span is less than two cache lines, the instruction buffer will hit and return the instruction data within one cycle since we have already fetch it before. When a branch or jump instruction is taken and the instruction corresponding to the destination address is not currently in instruction buffer, the instruction buffer will be flushed and send a request to ICache.

### 2.1.3 Interfaces

**if2ic/ic2if:** These interfaces are used for sending PC fetch requests from IF to instruction buffer and loop buffer. This interface uses an enable signal to send requests. This enable signal is held high until a response is received. There are 2 signals in if2ic interface:

1. pc: The address of the requested instruction.
2. valid: If this request is valid.

On the response side (ic2if), the main signals are:

1. inst/rvc_inst: The instruction data.
2. valid: Whether this response is valid.
3. is_rvc: Whether the instruction is RVC or not.
4. excp_cause: Contains the exception cause of the instruction, if     any.
5. excp_valid: Whether this instruction was found to have an     exception.

**if2id:** This interface contains all the data that is passed from IF to ID. It works using a valid/ready handshake. There are 2 signals in this interface.

1. inst: The instruction data.
2. pc: The PC of the instruction.

**cs2if_npc/ma2if_npc/ex2if_npc/id2if_npc:** These interfaces are used for sending PC redirection request to IF. They work using a valid/ready handshake. There are 2 signals in these interfaces.

1. pc: The new value of the PC register.
2. valid: Whether the request is valid.

### 2.1.4 Structure

![img](assets/RRV64_Processor_Databook/clip_image002.gif)

## 2.2  Decode

### 2.2.1 Introduce

Decode (ID) is the second stage in RRV64’s pipeline. It receives instruction data from the IF stage, expands RVC instructions, decodes instruction data to set the control signals, and sends requests to the regfile. When encountering an illegal instruction, the decoder will also generate an exception signal, which will be handled when the current instruction reaches the MA stage.

The RRV64 implements the standard compressed extension to the RISC-V architecture, which allows for 16-bit, in addition to the normal 32-bit instruction size. To handle this new size of instructions, ID contains a submodule that takes the 16-bit instructions and expand it to its 32-bit equivalent. This module acts as the first layer of decoding.

After ID has the final instruction data, either the expanded compressed instruction, or the initial instruction data, it will begin to decode the instruction to determine how to set the control signals that will be used throughout the pipeline. In the RTL code, you can find a case statement that will call different functions depending on the instruction’s opcode, funct7 field, funct5 field, etc. These functions will output the appropriate control signals.

There is a Regfile Scoreboard in this stage. Its purpose is to track which registers still have pending writes. This is used to resolve data hazards. When ID decodes that its instruction will eventually write to the regfile, it indexes into the scoreboard using rd (the index of the destination register) and marks that entry, to signal that there is a pending write, and thus a possible data hazard. When that instruction eventually writes to the regfile, that scoreboard entry is cleared. If ID has an instruction and with one, or both, of its source registers indicating pending writes, it will use the data pushed forward from EX stage or wait for the data retrieved from the memory.

### 2.2.2 Interface

**id2ex:** This interface contains all the data passed from ID to EX. It works on a valid/ready handshake. There are 6 signals in this interface.

1. pc: The PC of the instruction.
2. inst: The instruction data.
3. rs1_addr: The address of source register 1.
4. rs2_addr: The address of source register 2.
5. is_rvc: Signals whether this instruction is RVC, used to     calculate npc in EX and MA stage, if needed.

**ex2id_bps/ma2id_bps:** These interfaces are used for data forwarding: send the execution result of the EX/MA stage back to the EX stage to solve data hazard. There are 4 signals in this interface.

1. valid_addr: Indicating whether the address of register     accessing or memory accessing is valid.
2. valid_data: Indicating whether the data of register accessing     or memory accessing is valid.
3. addr: The address of register accessing or memory accessing.     Used to compare with the address to be accessed by the instruction in the     ID stage.
4. data: The data in register accessing or memory accessing.

### 2.2.3 Structure

![img](assets/RRV64_Processor_Databook/clip_image002-16312724681731.gif)



## 2.3  Executions

### 2.3.1 Introduction

The execute stage is responsible for calculations and sending memory requests to the LSU. This stage consists of an arithmetic and logic unit (ALU), a pair of multi-cycle multiplier and divider, a branch address calculation unit and a load/store address calculation unit.

l ALU: The ALU is responsible for additions, subtractions, shifts, data comparisons (for branches and slt instructions), and bit-wise logical operations (AND, OR, XOR). The ALU is fed with the operands as well as the operation type. The logic in ALU is purely combinational.

l Multiplier: The multiplier is used for multiplications. It is fed the operands as well as the multiplication type. The start_pulse input of the multiplier is set to 1 for 1 cycle to trigger the multiplication operation. The complete output is set to 1 when the multiplication is done. For multiplications where only the lower 64 bits of the result are needed, the calculation completes in the same cycle the start_pulse is set to 1. For multiplications where the upper 64 bits of the result are needed, the calculation completes in 3 cycles.

l Divider: The divider is used for division operations. The divider is fed with the operands as well and the division type. The divider triggers the calculation when start_pulse input is set to 1. The complete output is set to 1 when DIV is done. DIV takes 17 cycles to accomplish a division operation.

The target address of the branch and the address of load/store instructions are calculated by the branch address calculation unit. For a branch instruction, if the branch is taken, a flush signal will be sent to IF and ID to “flush” the instructions in those stage, and a redirection signal will be sent to IF and the value of PC will change accordingly. For load/store instruction, the memory access request will be sent to D-Cache, so if D-Cache hits, we can the get the memory access result at MA stage in the next cycle.

### 2.3.2 Interfaces

**ex2ma:** This interface contains all the data passed from EX to MA. It works on a valid/ready handshake. There are 6 signals in this interface.

1. pc: The PC of the instruction.
2. inst: The instruction data.
3. ex_out: The result of EX’s calculation.
4. rd_addr: The address of destination register 1, if any.
5. csr_addr: The address of csr register, if any.
6. is_rvc: Whether this instruction is RVC.

**ex2dc:** This is the interface between EX and D-Cache(L1 dcache is bypassed), used for sending memory requests. It uses a valid/ready handshake. There are 5 signals in this interface.

1. rw: 1 if the request is a write, 0 if it is a read.
2. mask: The byte mask for Store operation.
3. addr: The memory request address.
4. wdata: The write data of the memory request.
5. width: The width of the operand of Load/Store operation.

## 2.4  Memory Access

### 2.4.1 Introduction

This stage is responsible for receiving memory responses from D-Cache, interfacing with rrv_csr (CSR), sending redirection requests to IF in certain cases, and committing instructions and writing data to Register Files.

For load and store instructions, MA will receive memory responses from D-Cache. Only 1 memory response is accepted per instruction. Loads will respond with the data read from memory, while stores will respond with 0 data. The data will be pushed forward to the ID stage through the bypass network to solve possible data hazard.

For CSR instructions, the MA stage will read and write the CSR Registers.

For fence or those csr operations on the PMP related registers, MA will send a npc signal to the IF stage to release the stall state of the IF, ID and EX stages.

For instructions with destination register and without any exceptions, it is at MA stage that the result will write to the regfile. Regfile writes are synchronous.

### 2.4.2 Address Translation

To support an operating system, RRV64 features full hardware support for address translation via a Memory Management Unit (MMU). It has separate configurable data and instruction TLBs. The TLBs are fully set-associative memories. On each instruction and data access, they are checked for a valid address translation. If none exists, RRV64’s hardware PTW queries the main memory for a valid address translation. The replacement strategy of TLB entries is Pseudo Least Recently Used (LRU).

Both instruction cache and data cache are virtually indexed and physically tagged and fully parametrizable. The address is split into page offset (lower 12 bit) and virtual page number (bit 12 up to 39). The page offset is used to index into the cache while the virtual page number is simultaneously used for address translation through the TLB. In case of a TLB miss the pipeline is stalled until the translation is valid.

### 2.4.3 Interfaces

**dc2ma:** This interface is the memory response interface between D-Cache and MA. There are 4 signals in this interface.

1. rdata: The read data requested by load instructions.
2. excp_valid: Signals whether the memory access operation cause     an exception (e.g. violated a PMP check).
3. excp_cause: Contains the exception cause of the instruction, if     any.
4. valid: Whether the response is valid.

**ma2cs/ma2cs_ctrl:** These interfaces are used by MA for sending read/write requests to CSR. The ma2cs_ctrl is for controlling transactions with CSR. In ma2cs_ctrl, there are 3 signals in this interface:

1. csr_op: CSR operation type. It can be set to RRV64_CSR_OP_RW     (read and write), RRV64_CSR_OP_RS (read and set), RRV64_CSR_OP_RC (read     and clear) and CSR_OP_NONE if MA does not have a request to CSR.
2. ret_type: Return instruction type (mret or uret). It will be     set to RET_TYPE_NONE if the instruction is not either of the ret type     instructions mentioned.
3. is_wfi: Set to 1 if the instruction is a WFI instruction.

For ma2cs, there are 5 signals in this interface:

1. pc: PC of the current instruction. Used mainly for exception     handling.
2. csr_addr: Request CSR address.
3. csr_wdata: Data used for do some calculation with data in CSR,     the calculation result will be written back to the CSR.
4. rs1_addr: rs1 address of the instruction. Used for checking if     the CSR operation should be considered a write.
5. mem_addr: Memory address of the load or store instruction. Used     for updating the MTVAL CSR on load/store PMP exceptions.

**ma2irf:** This interface is used by MA to send regfile writes to IRF. Writes will be validated using an active high write enable signal. Including the enable signal, there are 3 signals in this interface:

1. rd: Write data.
2. rd_addr: Regfile write address.
3. rd_we: Write enable.

# 3  Exceptions and Interrupt 

We design a mechanism to handle trap, which will control pipeline stall / flush / jump under special circumstances (such as exception / interrupt / debugging, etc.). 

## 3.1  Exception

The handling of exceptions follows the provisions of riscv spec. The core handle five exceptions:

1. Illegal instruction, including:

   - Illegal instruction code

   - Accessing high privilege CSR registers at low privilege level. (e.g. access machine mode CSR registers under user mode) 

   - Accessing undefined CSR addr.

   - Trying to write read-only CSR registers

2. Store address misalignment

3. Load address misalignment

4. ECALL

5. EBREAK





--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------







# 3    Privilege Levels

RRV64 support 3 privilege mode:

- Machine mode
- Supervisor  mode
- User mode

# 4    Signal Description

## 4.1   Clock and Reset Signals

 

| **Signal**              | **I/O** | **Active** | **Width**   | **Description**                                         |
| ----------------------- | ------- | ---------- | ----------- | ------------------------------------------------------- |
| clk                     | I       | NA         | 1           | clock signal                                            |
| s2b_early_rst | I       | NA         | 1         | debug access logic's reset. must de-assert at least 1 cycle early then s2b_rst |
| s2b_rst       | I       | NA         | 1         | orb 64's none debug logic's reset                            |

## 4.2 Configure signals

| Signal                  | I/O  | Active | Width | Description                                                  |
| ----------------------- | ---- | ------ | ----- | ------------------------------------------------------------ |
| s2b_cfg_bypass_ic       | I    | NA     | 1     | l1 instruction cache work or bypass                          |
| s2b_cfg_bypass_tlb      | I    | NA     | 1     | itlb work and dtlb work or bypass                            |
| s2b_cfg_en_hpmcounter   | I    | NA     | 1     | hardware performance counter turn on or not                  |
| s2b_cfg_itb_en          | I    | NA     | 1     | instruction trace buffer work or not                         |
| s2b_cfg_itb_sel         | I    | NA     | 3     | pipeline stage trace select:  RRV64_ITB_SEL_IF = 3'h0  RRV64_ITB_SEL_ID = 3'h1  RRV64_ITB_SEL_EX = 3'h2  RRV64_ITB_SEL_MA = 3'h3  RRV64_ITB_SEL_WB = 3'h4 |
| s2b_cfg_itb_wrap_around | I    | NA     | 1     | when trace buffer is full，allow address wrap round to 0 or not |
| s2b_cfg_lfsr_seed       | I    | NA     | 6     | instruction cache way replace lfsr random seed               |
| s2b_cfg_pwr_on          | I    | NA     | 1     | instruction cache sram powe on/down                          |
| s2b_cfg_rst_pc          | I    | NA     | 39    | PC pointer reset value                                       |
| s2b_cfg_sleep           | I    | NA     | 1     | core sleep                                                   |

 

## 4.3   Debug and Breakpoint Signals

| Signal                | I/O  | Width | Description                                  |
| --------------------- | ---- | ----- | -------------------------------------------- |
| s2b_bp_if_pc_0[38:0]  | I    | 39    | 0 instruction  fetch stage PC match value    |
| s2b_bp_if_pc_1[38:0]  | I    | 39    | 1 instruction  fetch stage PC match value    |
| s2b_bp_if_pc_2[38:0]  | I    | 39    | 2 instruction  fetch stage PC match value    |
| s2b_bp_if_pc_3[38:0]  | I    | 39    | 3 instruction  fetch stage PC match value    |
| s2b_bp_instret[63:0]  | I    | 64    | minstret csr breakporint match value         |
| s2b_bp_wb_pc_0[38:0]  | I    | 39    | 0 write back  statge PC match value          |
| s2b_bp_wb_pc_1[38:0]  | I    | 39    | 1 write back  statge PC match value          |
| s2b_bp_wb_pc_2[38:0]  | I    | 39    | 2 write back  statge PC match value          |
| s2b_bp_wb_pc_3[38:0]  | I    | 39    | 3 write back  statge PC match value          |
| s2b_en_bp_if_pc_0     | I    | 1     | 0 instruction  fetch statge PC match enable  |
| s2b_en_bp_if_pc_1     | I    | 1     | 1 instruction  fetch statge PC match enable  |
| s2b_en_bp_if_pc_2     | I    | 1     | 2 instruction  fetch statge PC match enable  |
| s2b_en_bp_if_pc_3     | I    | 1     | 3 instruction  fetch statge PC match enable  |
| s2b_en_bp_instret     | I    | 1     | minstret csr breakporint match enable        |
| s2b_en_bp_wb_pc_0     | I    | 1     | 0 write back  statge PC match enable         |
| s2b_en_bp_wb_pc_1     | I    | 1     | 1 write back  statge PC match enable         |
| s2b_en_bp_wb_pc_2     | I    | 1     | 2 write back  statge PC match enable         |
| s2b_en_bp_wb_pc_3     | I    | 1     | 3 write back  statge PC match enable         |
| s2b_ext_event         | I    | 1     |                                              |
| b2s_debug_stall_out   | O    | 1     | core pipeline stall valid                    |
| b2s_if_pc[38:0]       | O    | 39    | instruction fetch PC value                   |
| b2s_itb_last_ptr[4:0] | O    | 5     | instruction trace buffer RAM address pointer |

## 4.4   Interrupt Signals

| **Signal** | **I/O** | **Active** | **Width** | **Description**    |
| ---------- | ------- | ---------- | --------- | ------------------ |
| ext_int    | I       | NA         | 1         | extern interrupt   |
| sw_int     | I       | NA         | 1         | software interrupt |
| timer_int  | I       | NA         | 1         | timer interrupt    |

## 4.5 L2 cache access Signals

RRV64 use this interface to do cacheable load/store memory access

| **Signal**              | **I/O** | **Active** | **Width** | **Description**                                              |
| ----------------------- | ------- | ---------- | --------- | ------------------------------------------------------------ |
| req_paddr[39:0]         | O       | NA         | 40        | l2 cache request address                                     |
| req_data[255:0]         | O       | NA         | 256       | l2 cache request write data                                  |
| req_mask[31:0]          | O       | NA         | 32        | l2 cache request write byte marsk                            |
| req_tid.cpu_noc_id[2:0] | O       | NA         | 3         | request id                                                   |
| req_tid.src[3:0]        | O       | NA         | 4         | request id                                                   |
| req_tid.tid[3:0]        | O       | NA         | 4         | request id                                                   |
| req_type[3:0]           | O       | NA         | 4         | REQ_LR              = 4'b1000,<br/>REQ_SC              = 4'b1001,<br/>REQ_BARRIER_SET     = 4'b1010,<br/>REQ_BARRIER_SYNC    = 4'b1011,<br/>REQ_READ            = 4'b0000,<br/>REQ_WRITE           = 4'b0010,<br/>REQ_WRITE_NO_ALLOC  = 4'b0011, <br/>REQ_AMO_LR          = 4'b0100, <br/>REQ_AMO_SC          = 4'b0101, <br/>REQ_FLUSH_IDX       = 4'b0110,<br/>REQ_FLUSH_ADDR      = 4'b0111 |
| l2_req_ready            | I       | NA         | 1         | when assert, means l1 cache is ready to access request       |
| l2_req_valid            | O       | NA         | 1         | cpu's l2 cache request valid                                 |

## 4.6 AXI4 Outbound system request

none cacheable memory address space and memory mapped IO space use this interface

| **Signal**                    | **I/O** | **Active** | **Width** | **Description** |
| ----------------------------- | ------- | ---------- | --------- | --------------- |
| sysbus_req_if_ar.arid[11:0]   | O       | NA         | 12        | See AXI4        |
| sysbus_req_if_ar.araddr[39:0] | O       | NA         | 40        |                 |
| sysbus_req_if_arready         | I       | NA         | 1         |                 |
| sysbus_req_if_arvalid         | O       | NA         | 1         |                 |
| sysbus_req_if_aw.awid[11:0]   | O       | NA         | 12        |                 |
| sysbus_req_if_aw.awaddr[39:0] | O       | NA         | 40        |                 |
| sysbus_req_if_awready         | I       | NA         | 1         |                 |
| sysbus_req_if_awvalid         | O       | NA         | 1         |                 |
| sysbus_req_if_w.wdata[63:0]   | O       | NA         | 64        |                 |
| sysbus_req_if_w.wstrb[7:0]    | O       | NA         | 8         |                 |
| sysbus_req_if_w.wlast         | O       | NA         | 1         |                 |
| sysbus_req_if_wready          | I       | NA         | 1         |                 |
| sysbus_req_if_wvalid          | O       | NA         | 1         |                 |
| sysbus_resp_if_b.bid[11:0]    | I       | NA         | 12        |                 |
| sysbus_resp_if_b.bresp[1:0]   | I       | NA         | 2         |                 |
| sysbus_resp_if_bready         | O       | NA         | 1         |                 |
| sysbus_resp_if_bvalid         | I       | NA         | 1         |                 |
| sysbus_resp_if_r              | I       | NA         | 1         |                 |
| sysbus_resp_if_r.rid[11:0]    | I       | NA         | 12        |                 |
| sysbus_resp_if_r.rdata[63:0]  | I       | NA         | 64        |                 |
| sysbus_resp_if_rresp[1:0]     | I       | NA         | 2         |                 |
| sysbus_resp_if_r.rlast        | I       | NA         | 1         |                 |
| sysbus_resp_if_rready         | O       | NA         | 1         |                 |
| sysbus_resp_if_rvalid         | I       | NA         | 1         |                 |

## 4.7   AXI4 Inbound system request

This system interface is used to access RRV64 core's internal address pace. Such as icache/dcache/csr.



| **Signal**                  | **I/O** | **Active** | **Width** | **Description** |
| --------------------------- | ------- | ---------- | --------- | --------------- |
| ring_req_if_ar.arid[11:0]   | O       | NA         | 12        | See AXI4        |
| ring_req_if_ar.araddr[39:0] | O       | NA         | 40        |                 |
| ring_req_if_arready         | I       | NA         | 1         |                 |
| ring_req_if_arvalid         | O       | NA         | 1         |                 |
| ring_req_if_aw.awid[11:0]   | O       | NA         | 12        |                 |
| ring_req_if_aw.awaddr[39:0] | O       | NA         | 40        |                 |
| ring_req_if_awready         | I       | NA         | 1         |                 |
| ring_req_if_awvalid         | O       | NA         | 1         |                 |
| ring_req_if_w.wdata[63:0]   | O       | NA         | 64        |                 |
| ring_req_if_w.wstrb[7:0]    | O       | NA         | 8         |                 |
| ring_req_if_w.wlast         | O       | NA         | 1         |                 |
| ring_req_if_wready          | I       | NA         | 1         |                 |
| ring_req_if_wvalid          | O       | NA         | 1         |                 |
| ring_resp_if_b.bid[11:0]    | I       | NA         | 12        |                 |
| ring_resp_if_b.bresp[1:0]   | I       | NA         | 2         |                 |
| ring_resp_if_bready         | O       | NA         | 1         |                 |
| ring_resp_if_bvalid         | I       | NA         | 1         |                 |
| ring_resp_if_r              | I       | NA         | 1         |                 |
| ring_resp_if_r.rid[11:0]    | I       | NA         | 12        |                 |
| ring_resp_if_r.rdata[63:0]  | I       | NA         | 64        |                 |
| ring_resp_if_rresp[1:0]     | I       | NA         | 2         |                 |
| ring_resp_if_r.rlast        | I       | NA         | 1         |                 |
| ring_resp_if_rready         | O       | NA         | 1         |                 |
| ring_resp_if_rvalid         | I       | NA         | 1         |                 |





# 5    Reset and Clocking

| **Port Name** | **I/O** | **Description**                                              |
| :------------ | :------ | :----------------------------------------------------------- |
| clk           | I       | RRV64 clock, all logic runs on this clock.                   |
| s2b_rst       | I       | Active high reset signal.0 - Takes core out of reset1 - Resets the core |
| s2b_early_rst | I       | Active high early reset signal, controls debug access logic. |

# 6   Caches

## 6.1   Instruction Cache

instruction cache can be configured to 2 way set associate mode. replace policy choose LFSR and it's seed can be configure.

Instruction cache's default size is 8KB and can be changed to 8,16,32,64  KB.

## 6.2   Data Cache

no l1 data cache. bypass mode.

# 7 Buses

l2 request bus: used to access cacheable memory space. such as instructions and data.

sysbus request bus: used to do uncachealbe memory space or mempory mapped IO space.

ring request bus: used to access core's internal memory, such as icache/dache/csr.

# 8 Exceptions and Interrupt 

## 8.1     Exception

The handling of exceptions follows the provisions of riscv spec. at present, five exception causes are reported:

### 1. Illegal instruction

This type of error contains a variety of exceptions, and the chip is not distinguished

1. Instruction not recognized;
2. Access CSR with high permission, for example, the kernel accesses the machine mode register when the user mode permission;
3. Accessing the disabled CSR counter;
4. Access unsupported CSR addresses;
5. CSR read / write operation is incorrect, for example, a read-only register is written;
6. Illegal routing is configured in the floating instruction;
7. The floating instruction not enabled;

### 2. Store address misalignment

store address not aligned to 2/4Byte。

### 3. Load address misalignment

load address not aligned to 2/4Byte

### 4. ECALL

exception trap instrucion

### 5. EBREAK

exception break instruction. used for kernel debug program.

## 8.2     Interrupt

The processing of interrupt follows the provisions of riscv spec. at present, the kernel does not process the interrupt of user mode. The three interrupts supported are as follows:

1. Machine software interrupt: software triggers an interrupt.

2. Machine timer interrupt: the timer in the chip triggers an interrupt.

3. Machine external interrupt: includes external fast interrupt and ordinary interrupt.

# 9 Debug System

Debug-socket is proxy running on host to interact with target, the functionality of debug-socket in software development, as shown in the following picture. 

![](assets/RRV64_Processor_Databook/Socket_debug_in_SW_development.png)

RiVAI choose to use a software-based debug socket instead of a standard debug module to implement the debug function, both of which have the same effect and can be used for debugging of the soc. For RiVAI’s debug-socket, see debug-socket connections overview.

![](assets/RRV64_Processor_Databook/Debug_socket_connection_overview.png)

Basically, RiVAI debug-socket implements basic functions required by gdb, with the help of hardware-provided breakpoint, watchpoint, trace buffer, and many other features.

 

# 10 Timer

| Register    | Width | Description                                                  |
| ----------- | ----- | ------------------------------------------------------------ |
| timer_en    | 1     | timer enable signal, timer will counting  up when this signal is high after reset. |
| timer       | 64    | timer counter, read / write, reset to  zero.                 |
| timercmp[n] | 64    | n is the index of the hart, start from 0.  timercmp will be reset to all 1. |

Please refer <<The RISC-V Instruction Set Manual

Volume II: Privileged Architecture

Document Version 20190608-Priv-MSU-Ratifi ed>> 

Chapter 3.1.10 for detailed description.

# 11 Memory Mapped Registers

| ADDRESS                                   | W    | reset value | TYPE | DESCRIPTION                                                  |
| ----------------------------------------- | ---- | ----------- | ---- | ------------------------------------------------------------ |
| STATION_VP_S2B_CFG_RST_PC_ADDR_0          | 64   | 0           | RW   | core'0s reset PC                                             |
| STATION_VP_S2B_CFG_RST_PC_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_RST_PC_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_RST_PC_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_SEL_ADDR_0         | 64   | 0           | RW   | pipeline stage trace select:  RRV64_ITB_SEL_IF = 3'h0  RRV64_ITB_SEL_ID = 3'h1  RRV64_ITB_SEL_EX = 3'h2  RRV64_ITB_SEL_MA = 3'h3  RRV64_ITB_SEL_WB = 3'h4 |
| STATION_VP_S2B_CFG_ITB_SEL_ADDR_1         | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_SEL_ADDR_2         | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_SEL_ADDR_3         | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_LFSR_SEED_ADDR_0       | 64   | 0           | RW   | core0's icache way select LFSR's rand seed                   |
| STATION_VP_S2B_CFG_LFSR_SEED_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_LFSR_SEED_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_LFSR_SEED_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_EN_HPMCOUNTER_ADDR_0   | 64   | 0           | RW   | core0's HPM counter enable                                   |
| STATION_VP_S2B_CFG_EN_HPMCOUNTER_ADDR_1   | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_EN_HPMCOUNTER_ADDR_2   | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_EN_HPMCOUNTER_ADDR_3   | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_PWR_ON_ADDR_0          | 64   | 0           | RW   | core's power on control                                      |
| STATION_VP_S2B_CFG_PWR_ON_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_PWR_ON_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_PWR_ON_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_SLEEP_ADDR_0           | 64   | 0           | RW   | core's sleep control                                         |
| STATION_VP_S2B_CFG_SLEEP_ADDR_1           | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_SLEEP_ADDR_2           | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_SLEEP_ADDR_3           | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_BYPASS_IC_ADDR_0       | 64   | 0           | RW   | core0's l1 icache bypass control                             |
| STATION_VP_S2B_CFG_BYPASS_IC_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_BYPASS_IC_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_BYPASS_IC_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_BYPASS_TLB_ADDR_0      | 64   | 0           | RW   | core0's TLB bypass control                                   |
| STATION_VP_S2B_CFG_BYPASS_TLB_ADDR_1      | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_BYPASS_TLB_ADDR_2      | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_BYPASS_TLB_ADDR_3      | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_EN_ADDR_0          | 64   | 0           | RW   | core0's instrution trace buffer enable                       |
| STATION_VP_S2B_CFG_ITB_EN_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_EN_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_EN_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_WRAP_AROUND_ADDR_0 | 64   | 0           | RW   | core0's instrution trace buffer write address wrap back to 0 enable |
| STATION_VP_S2B_CFG_ITB_WRAP_AROUND_ADDR_1 | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_WRAP_AROUND_ADDR_2 | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_CFG_ITB_WRAP_AROUND_ADDR_3 | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_0_ADDR_0          | 64   | 0           | RW   | core0's IF stage breakpoint PC0                              |
| STATION_VP_S2B_BP_IF_PC_0_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_0_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_0_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_1_ADDR_0          | 64   | 0           | RW   | core0's IF stage breakpoint PC1                              |
| STATION_VP_S2B_BP_IF_PC_1_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_1_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_1_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_2_ADDR_0          | 64   | 0           | RW   | core0's IF stage breakpoint PC2                              |
| STATION_VP_S2B_BP_IF_PC_2_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_2_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_2_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_3_ADDR_0          | 64   | 0           | RW   | core0's IF stage breakpoint PC3                              |
| STATION_VP_S2B_BP_IF_PC_3_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_3_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_IF_PC_3_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_0_ADDR_0          | 64   | 0           | RW   | core0's WB stage breakpoint PC0                              |
| STATION_VP_S2B_BP_WB_PC_0_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_0_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_0_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_1_ADDR_0          | 64   | 0           | RW   | core0's WB stage breakpoint PC1                              |
| STATION_VP_S2B_BP_WB_PC_1_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_1_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_1_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_2_ADDR_0          | 64   | 0           | RW   | core0's WB stage breakpoint PC2                              |
| STATION_VP_S2B_BP_WB_PC_2_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_2_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_2_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_3_ADDR_0          | 64   | 0           | RW   | core0's WB stage breakpoint PC3                              |
| STATION_VP_S2B_BP_WB_PC_3_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_3_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_WB_PC_3_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_INSTRET_ADDR_0          | 64   | 0           | RW   | core0's minstret csr breakporint match value                 |
| STATION_VP_S2B_BP_INSTRET_ADDR_1          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_INSTRET_ADDR_2          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_BP_INSTRET_ADDR_3          | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_0_ADDR_0       | 64   | 0           | RW   | core0's IF stage break point PC0                             |
| STATION_VP_S2B_EN_BP_IF_PC_0_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_0_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_0_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_1_ADDR_0       | 64   | 0           | RW   | core0's IF stage break point PC1                             |
| STATION_VP_S2B_EN_BP_IF_PC_1_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_1_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_1_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_2_ADDR_0       | 64   | 0           | RW   | core0's IF stage break point PC2                             |
| STATION_VP_S2B_EN_BP_IF_PC_2_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_2_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_2_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_3_ADDR_0       | 64   | 0           | RW   | core0's IF stage break point PC3                             |
| STATION_VP_S2B_EN_BP_IF_PC_3_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_3_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_IF_PC_3_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_0_ADDR_0       | 64   | 0           | RW   | core0's WB stage break point PC0                             |
| STATION_VP_S2B_EN_BP_WB_PC_0_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_0_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_0_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_1_ADDR_0       | 64   | 0           | RW   | core0's WB stage break point PC1                             |
| STATION_VP_S2B_EN_BP_WB_PC_1_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_1_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_1_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_2_ADDR_0       | 64   | 0           | RW   | core0's WB stage break point PC2                             |
| STATION_VP_S2B_EN_BP_WB_PC_2_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_2_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_2_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_3_ADDR_0       | 64   | 0           | RW   | core0's WB stage break point PC3                             |
| STATION_VP_S2B_EN_BP_WB_PC_3_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_3_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_WB_PC_3_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_INSTRET_ADDR_0       | 64   | 0           | RW   | core0's minstret csr breakporint match enable                |
| STATION_VP_S2B_EN_BP_INSTRET_ADDR_1       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_INSTRET_ADDR_2       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_EN_BP_INSTRET_ADDR_3       | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_DEBUG_STALL_ADDR_0         | 64   | 0           | RW   | core0's debug stall control                                  |
| STATION_VP_S2B_DEBUG_STALL_ADDR_1         | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_DEBUG_STALL_ADDR_2         | 64   | 0           | RW   |                                                              |
| STATION_VP_S2B_DEBUG_STALL_ADDR_3         | 64   | 0           | RW   |                                                              |
| STATION_VP_B2S_DEBUG_STALL_OUT_ADDR_0     | 64   | 0           | RW   | core0's debug stall status                                   |
| STATION_VP_B2S_DEBUG_STALL_OUT_ADDR_1     | 64   | 0           | RW   |                                                              |
| STATION_VP_B2S_DEBUG_STALL_OUT_ADDR_2     | 64   | 0           | RW   |                                                              |
| STATION_VP_B2S_DEBUG_STALL_OUT_ADDR_3     | 64   | 0           | RW   |                                                              |
